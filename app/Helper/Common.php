<?php
namespace App\Helper;

class Common {
    /**
     * Clear XSS array inputs
     * @param $array
     * @return array
     */
    public static function clearXSS($array)
    {
        $inputs = array();
        foreach ($array as $key => $item) {
            if(is_array($item)) {
                $inputs[$key] = self::clearXSS($item);
            } else {
                $inputs[$key] = self::clearInputXSS($item);
            }
        }

        return $inputs;
    }
    /**
     * Clear data input
     *
     * @param $string
     * @return string
     */
    public static function clearInputXSS($string)
    {
        $string = nl2br($string);
        $string = trim(strip_tags($string));
        $string = self::removeScripts($string);
        return $string;
    }

    /**
     * Clear scripts input
     *
     * @param $string
     * @return string
     */
    public static function removeScripts($string)
    {
        $regex =
            '/(<link[^>]+rel="[^"]*stylesheet"[^>]*>)|' .
            '<script[^>]*>.*?<\/script>|' .
            '<style[^>]*>.*?<\/style>|' .
            '<!--.*?-->/is';

        return preg_replace($regex, '', $string);
    }

    public static function convertDuration($duration)
    {
        if($duration == 1) {
            $duration = '1 day';
        }
        else if($duration > 1) {
            $duration = $duration . ' days - ' . ($duration-1) . ' night';
        }
        return $duration;
    }
}
