<?php

define('UPLOAD_DIRECTORY', 'public\upload');

define('BOOKING_COMPLETE_STATUS', 4);

define('MONTHLY_TYPE', 'monthly');
define('WEEKLY_TYPE', 'weekly');
define('DAILY_TYPE', 'daily');
define('YEARLY_TYPE', 'yearly');
