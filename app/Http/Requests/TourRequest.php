<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TourRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        if(Auth::check()) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'image' => $this->customRuleImage(),
            'title' => 'required|string|max:255',
            'slug' => 'required|string|max:255|unique:tours,slug,'.$this->id,
            'destination_id' => 'required',
            'type_tour_id' => 'required',
            'price' => 'required|integer',
            'status' => 'required'
        ];
    }

    public function customRuleImage() {
        if($this->method() == 'POST') {
            $imageRule = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10000';
        }

        if($this->method() == 'PUT') {
            $imageRule = 'image|mimes:jpeg,png,jpg,gif,svg|max:10000';
        }
        return $imageRule;
    }
}
