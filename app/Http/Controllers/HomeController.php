<?php

namespace App\Http\Controllers;

use App\Services\DestinationService;
use App\Services\TourService;
use App\Services\TypeTourService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $destination;
    protected $tour;
    protected $typeTour;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        DestinationService $destination,
        TourService $tour,
        TypeTourService $typeTour
    )
    {
        $this->destination = $destination;
        $this->tour = $tour;
        $this->typeTour = $typeTour;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $typeTours = $this->typeTour->getAllTypeTours();
        $destinations = $this->destination->getAllDestinations();
        $tours = $this->tour->getAllTours();

        return view('homepage', compact('destinations', 'typeTours', 'tours'));
    }

    public function tour(Request $request)
    {
        $typeTours = $this->typeTour->getAllTypeTours();
        $tours = $this->tour->getAllTours(20);
        if($request->budget_max) {
            $tours = $this->tour->filter($request)->paginate(12);
        }
        //$review = $this->review;
        return view('pages.list_tour', compact('typeTours', 'tours'));
    }

    public function tourDetail($slug)
    {
        $tour = $this->tour->getTour($slug, true);
        if(!empty($tour)) {
            if(!empty($tour->description)) {
                $tour->description = json_decode($tour->description);
            }
            $toursRelateds = $this->destination->getDestination($tour->destination_id)->tours;

            //$rating = $this->review->getInfoRating($tour->id);
            //$reviews = $this->review;

            //$this->dispatch(new UpdateViewTourJob($tour->id));

            return view('pages.tour_detail', compact('tour', 'toursRelateds'));
        }
    }

    public function checkout()
    {
        if(Session('Booking')) {
            $booking = Session('Booking');
            return view('pages.checkout', compact('booking'));
        }
    }
}
