<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DeviceToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public $device;

    public function __construct(DeviceToken $device)
    {
        $this->middleware('test');
        $this->device  = $device;
    }

    public function index()
    {
//        $totalPay = number_format($this->booking->totalPay());
//        $totalBooking = $this->booking->getTotalBooking();
//        $totalTour = $this->tour->getTotalTour();
//        $totalDestination = $this->destination->getTotalDestination();
//        $totalContact = $this->contact->getTotalcontact();
//        $topViewTours = $this->tour->getTopViewTours();
        $this->authorize('view-dashboard');
        $totalPay = 0;
        $totalBooking = 0;
        $totalTour = 0;
        $totalDestination = 0;
        $totalContact = 0;
        $topViewTours = array();
        return view('admin.dashboard', compact('totalPay', 'totalBooking', 'totalTour', 'totalDestination', 'totalContact', 'topViewTours'));
    }

    public function updateDeviceToken(Request $request)
    {
        $currentUser = Auth::user();
        try {
            $deviceTokens = $this->device->updateOrCreate(['user_id' => $currentUser->id], [
                'user_id' => $currentUser->id,
                'token' => $request->token
            ]);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }

    }
}
