<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DestinationRequest;
use App\Services\DestinationService;
use Illuminate\Http\Request;

class DestinationController extends Controller
{
    protected $destination;
    public function __construct(DestinationService $destination)
    {
        $this->destination = $destination;
    }

    public function index()
    {
        return view('admin.destination.list');
    }

    public function getData(Request $request)
    {
        return $this->destination->getDataTable($request);
    }

    public function create()
    {
//        header('Cache-Control: no-store, no-cache, must-revalidate');
//        header('Expires: Thu, 01 Jan 1970 00:00:00 GMT');
//        header('Pragma: no-cache');
        return view('admin.destination.create');
    }

    public function store(DestinationRequest $request)
    {
        try {
//            $this->destination->save($request);
            return redirect()->route('test');
//            if($request->submit == 'submit') {
//                return back()->with('message', __('create_success', ['model' => 'Destination']));
//            }
//            return redirect()->route('destination.index')->with('message', __('create_success', ['model' => 'Destination']));
        } catch (\Exception $e) {
            return back()->withInput()->with('error', 'Destination added failed');
        }
    }

    public function edit($id)
    {
        $data = $this->destination->getDestination($id);
        return view('admin.destination.edit', compact('data'));
    }

    public function update(DestinationRequest $request, $id)
    {
        try {
            $this->destination->save($request, $id);
            return redirect()->route('destination.index')->with('message', 'Destination updated successfully');
        } catch (\Exception $e) {
            return back()->with('error', 'Destination updated failed');
        }
    }

    public function destroy($id)
    {
        $this->destination->delete($id);
    }
}
