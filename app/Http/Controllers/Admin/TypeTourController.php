<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\TypeTourRequest;
use App\Services\TypeTourService;
use Illuminate\Http\Request;

class TypeTourController extends Controller
{
    protected $typeTour;

    public function __construct(TypeTourService $typeTour)
    {
        $this->typeTour = $typeTour;
    }
    /*
     * Get data for datatables
     */
    public function getData(Request $request)
    {
        return $this->typeTour->getDataTables($request);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.type-tour.list');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Expires: Thu, 01 Jan 1970 00:00:00 GMT');
        header('Pragma: no-cache');
        return view('admin.type-tour.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TypeTourRequest $request)
    {
        try {
            $this->typeTour->save($request);
            if($request->submit == 'submit') {
                return back()->with('message', 'Type of tour added successfully');
            }
            return redirect()->route('type_tour.index')->with('message', 'Type of tour added successfully');
        } catch (\Exception $e) {
            return redirect()->route('type_tour.create')->withInput()->with('error', 'Type of tour added failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = $this->typeTour->getTypeTour($id);
        return view('admin.type-tour.edit',)->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TypeTourRequest $request, string $id)
    {
        try {
            $this->typeTour->save($request, $id);
            return redirect()->route('type_tour.index')->with('message', 'Type of tour updated successfully');
        } catch (\Exception $e) {
            return redirect()->route('type_tour.edit', $id)->with('error', 'Type of tour updated failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->typeTour->delete($id);
    }
}
