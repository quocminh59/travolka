<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tour;
use App\Services\DestinationService;
use App\Services\TourService;
use App\Services\TypeTourService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TourController extends Controller
{
    protected $tour;
    protected $typeTour;
    protected $destination;

    public function __construct(TourService $tour, TypeTourService $typeTour, DestinationService $destination)
    {
        $this->authorizeResource(Tour::class);
        $this->tour = $tour;
        $this->typeTour = $typeTour;
        $this->destination = $destination;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $destinations = $this->destination->getAllDestinations();
        $typeTours = $this->typeTour->getAllTypeTours();
        return view('admin.tour.list', compact('destinations', 'typeTours'));
    }

    /**
     * Get data for datatables
     */
    public function getData(Request $request)
    {
        return $this->tour->getDataTable($request);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $destination = $this->destination->getAllDestinations();
        $typeTour = $this->typeTour->getAllTypeTours();
        return view('admin.tour.create', compact('destination', 'typeTour'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $this->tour->save($request);
            if($request->submit == 'submit') {
                return back()->with('message', 'Tour added successfully');
            }
            return redirect()->route('tour.index')->with('message', 'Tour added successfully');
        } catch (\Exception $e) {
            return back()->withInput()->with('error', 'Tour added failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = $this->tour->getTour($id);
        $data->description = json_decode($data->description);
        return view('admin.tour.edit', ['destination' => $this->destination, 'typeTour' => $this->typeTour, 'data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            $this->tour->save($request, $id);
            return redirect()->route('tour.index')->with('message', 'Tour updated successfully');
        } catch (\Exception $e) {
            return redirect()->route('tour.edit', $id)->with('error', 'Tour updated failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->tour->delete($id);
    }
}
