<?php

namespace App\Services;

class FcmService implements Contracts\NotificationService
{

    const API_FCM_SEND_URL = 'https://fcm.googleapis.com/fcm/send';
    public function sendBatchNotification($deviceTokens, $data = null)
    {
        // TODO: Implement sendBatchNotification() method.
    }

    public function sendNotification($data, $topicName = null)
    {

    }

    public function subscribeTopic($deviceTokens, $topicName)
    {
        // TODO: Implement subscribeTopic() method.
    }

    public function unsubscribeTopic($deviceTokens, $topicName)
    {
        // TODO: Implement unsubscribeTopic() method.
    }
}
