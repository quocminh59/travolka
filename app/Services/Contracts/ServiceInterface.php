<?php

namespace App\Services\Contracts;

interface ServiceInterface
{
    public function model();
}
