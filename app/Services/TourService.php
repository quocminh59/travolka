<?php

namespace App\Services;

use App\Helper\Common;
use App\Models\Tour;
use Yajra\DataTables\DataTables;

class TourService
{
    protected $tour;

    public function __construct(Tour $tour)
    {
        $this->tour = $tour;
    }

    public function getDataTable($request)
    {
        $data = $this->tour->filter($request);
        return Datatables::of($data)
            ->addIndexColumn()
            ->limit(function($data) use ($request) {
                return $data->where('id', '>', $request->start);
            })
            ->addColumn('image', function($data) {
                $pathImage = asset('storage/upload/'.$data->image);
                return "<img  class='tb-image' src='".$pathImage."' />";
            })
            ->addColumn('trending', function($data) {
                $url = route('tour.status', ['id' => $data->id]);
                return view('admin.elements.switch_trending', compact('data','url'));
            })
            ->addColumn('status', function($data) {
                $url = route('tour.status', ['id' => $data->id]);
                return view('admin.elements.switch', compact('data','url'));
            })
            ->addColumn('action', function($data) {
                $id = $data->id;
                return view('admin.elements.act_tour', ['id' => $id]);
            })
            ->addColumn('more', function($data) {
                $id = $data->id;
                return view('admin.elements.more_act', compact('id'));
            })
            ->rawColumns(['image', 'status'])
            ->make(true);
    }

    public function save($request, $id = 0)
    {
        $requestData = Common::clearXSS($request->except('image'));

        if($request->image) {
            // storage image
            $imageUpload = $request->image->store(UPLOAD_DIRECTORY);
            $requestData['image'] = basename($imageUpload);
        }

        $description = [
            'overview' => $requestData['overview'],
            'included' => $requestData['included'],
            'departure' => $requestData['departure']
        ];
        $requestData['description'] = json_encode($description);

        return $this->tour->updateOrCreate(['id' => $id], $requestData);
    }

    public function getTour($field, $queryBySlug = false)
    {
        if($queryBySlug) {
            return $this->tour->getTourBySlug($field);
        }

        return $this->tour->getTourByID($field);
    }

    public function delete($id)
    {
        $tour = $this->getTour($id);
        $tour->delete();
    }

    public function getAllTours($perPage = 10)
    {
        return $this->tour->latest()->paginate($perPage);
    }
}
