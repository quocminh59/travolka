<?php

namespace App\Services;

use App\Helper\Common;
use App\Models\TypeTour;
use Yajra\DataTables\DataTables;

class TypeTourService
{
    protected $typeTour;

    public function __construct(TypeTour $typeTour)
    {
        $this->typeTour = $typeTour;
    }

    public function getDataTables($request)
    {
        $data = $this->typeTour->filter($request);

        return Datatables::of($data)
            ->addIndexColumn()
            ->limit(function($data) use ($request) {
                return $data->where('id', '>', $request->start);
            })
            ->addColumn('status', function($data) {
                $url = route('type_tour.status', ['id' => $data->id]);
                return view('admin.elements.switch', compact('data','url'));
            })
            ->addColumn('action', function($data) {
                $id = $data->id;
                return view('admin.elements.act_type_tour', ['id' => $id]);
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    public function save($request, $id = 0)
    {
        $requestData = Common::clearXSS($request->all());

        return $this->typeTour->updateOrCreate(['id' => $id], $requestData);
    }

    public function delete($id)
    {
        $typeTour = $this->getTypeTour($id);
        $typeTour->delete();
    }

    public function getTypeTour($field, $queryBySlug = false)
    {
        if($queryBySlug) {
            $typeTour = $this->typeTour->getTypeTourBySlug($field);
        } else {
            $typeTour = $this->typeTour->getTypeTourByID($field);
        }

        return $typeTour;
    }

    public function getAllTypeTours()
    {
        return $this->typeTour->all();
    }
}
