<?php

namespace App\Services;

use App\Helper\Common;
use App\Models\Destination;
use Yajra\Datatables\Datatables;
class DestinationService
{
    protected $destination;
    public function __construct(Destination $destination)
    {
        $this->destination = $destination;
    }

    public function getDataTable($request)
    {
        $data = $this->destination->latest();
//        dd($data);
        return Datatables::of($data)
            ->addIndexColumn()
            ->limit(function ($data) {
                $data->where('id', '>', request('start'));
            })
            ->addColumn('image', function($data) {
                $pathImage = asset('storage/upload/'.$data->image);
                return "<img  class='tb-image' src='".$data->image."' />";
            })
            ->addColumn('status', function($data) {
                $url = route('destination.status', ['id' => $data->id]);
                return view('admin.elements.switch', compact('data','url'));
            })
            ->addColumn('action', function($data) {
                $id = $data->id;
                return view('admin.elements.act_des', ['id' => $id]);
            })
            ->rawColumns(['image', 'status'])
            ->make();
    }

    public function save($request, $id = 0)
    {
        $requestData = Common::clearXSS($request->except('image'));

        if(!empty($request->image)) {
            $uploadImage = $request->image->store(UPLOAD_DIRECTORY);
            $requestData['image'] = basename($uploadImage);
        }

        return $this->destination->updateOrCreate(['id' => $id], $requestData);
    }

    public function delete($id)
    {
        $destination = $this->getDestination($id);
        $destination->delete();
    }

    public function getDestination($field, $queryBySlug = false)
    {
        if($queryBySlug) {
            $destination = $this->destination->getDestinationBySlug($field);
        } else {
            $destination = $this->destination->getDestinationByID($field);
        }

        return $destination;
    }

    public function getAllDestinations($perPage = 10)
    {
        return $this->destination->latest()->paginate($perPage);
    }

}
