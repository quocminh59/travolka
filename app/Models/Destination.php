<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'slug',
        'image',
        'status'
    ];

    public function tours()
    {
        return $this->hasMany(Tour::class);
    }

    public function getDestinationBySlug($slug)
    {
        return $this->where('slug', $slug)->first();
    }

    public function getDestinationByID($id)
    {
        return $this->find($id);
    }
}
