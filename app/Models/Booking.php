<?php

namespace App\Models;

use App\Events\BookingPaidEvent;
use App\Notifications\BookingComplete;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Notification;
use Yajra\Datatables\Datatables;
use Session;
use Carbon\Carbon;
use App\Models\Mail;
use App\Models\Tour;

class Booking extends Model
{
    use HasFactory;

    const BOOKING_COMPLETED = 4;
    const BOOKING_CANCELD = 3;
    const PAID = 2;
    const UNPAID = 1;

    protected $guarded = ['id'];

    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }

    public function getDataAjax($request)
    {
        $data = $this->select('*')->latest();
        if($request->ajax()) {
            return Datatables::of($data)
                ->addIndexColumn()
                ->filter(function ($query) use ($request) {
                    $this->customFilterDataTable($query, $request);
                })
                ->addColumn('fullname', function($data) {
                    $fullname = $data->first_name.' '.$data->last_name;
                    return $fullname;
                })
                ->addColumn('status', function($data) {
                    $data = $this->formatBookingData($data);
                    return $data->status;
                })
                ->addColumn('payment_status', function($data) {
                    $url = route('booking.status_payment', $data->id);
                    return view('admin.elements.status_payment', compact('data', 'url'));
                })
                ->addColumn('action', function($data) {
                    return view('admin.elements.act_booking', ['id' => $data->id]);
                })
                ->rawColumns(['fullname', 'status', 'action', 'payment_status'])
                ->make(true);
        }
    }

    public function customFilterDataTable($query, $request)
    {
        if(!empty($request->search)) {
            $query->where('booking_code', 'like', "%$request->search%")
                ->orWhere('first_name', 'like', "%$request->search%")
                ->orWhere('last_name', 'like', "%$request->search%")
                ->orWhere('phone', $request->search)
                ->orWhere('email', $request->search);
        }
        if(!empty($request->status)) {
            $query->where('status', $request->status);
        }
        if(!empty($request->departure_date)) {
            $query->where('departure_date', $request->departure_date);
        }
        if(!empty($request->payment_status)) {
            $query->where('payment_status', $request->payment_status);
        }
    }

    public function createBookingSession($request)
    {
        $this->resetSession();
        $request->session()->put('Booking', $request->all());
    }

    public function resetSession()
    {
        if(Session('Booking')) {
            session()->forget('Booking');
        }
    }

    public function saveRecord($request, $booking)
    {
        try {
            $departure_date = Carbon::createFromFormat('d/m/Y', $booking['departure_date'])->format('Y-m-d');
            $request->request->add(['departure_date' => $departure_date]);
            $request->request->add(['tour_id' => $booking['tour_id']]);
            $request->request->add(['number_people' => $booking['number_people']]);
            $request->request->add(['total_price' => $booking['total_price']]);
            $model = $this->create($request->all());
            $this->generateBookingCode($model->id);
            $this->resetSession();
            //$this->sendMailBooking($model->id);
            BookingPaidEvent::dispatch($model);
            return $model;
        } catch (\Throwable $th) {
            return 0;
        }
    }

    public function generateBookingCode($id)
    {
        $model = $this->findOrFail($id);
        $model->booking_code = 'nd-'.str_pad($id, 4, '0', STR_PAD_LEFT);
        $model->save();
    }

    public function sendMailBooking($id)
    {
        $booking = $this->findOrFail($id);
        $booking = $this->formatBookingData($booking);
        Notification::route('mail', 'quanquocminh@gmail.com')->notify(new BookingComplete($booking));

    }

    public function getBookingById($id)
    {
        return $this->findOrFail($id);
    }

    public function formatBookingData($booking)
    {
        $booking->departure_date = Carbon::createFromFormat('Y-m-d', $booking['departure_date'])->format('m/d/Y');
        switch ($booking->status) {
            case '2':
                $booking->status = 'Confirmed';
                break;
            case '3':
                $booking->status = 'Cancel';
                break;
            case '4':
                $booking->status = 'Completed';
                break;
            default:
                $booking->status = 'New';
                break;
        }
        switch ($booking->payment_method) {
            case '1':
                $booking->payment_method = 'Credit Card';
                break;
            case '2':
                $booking->payment_method = 'Paypal';
                break;
            default:
                $booking->payment_method = 'Pay in cash';
                break;
        }
        switch ($booking->payment_status) {
            case '2':
                $booking->payment_status = 'paid';
                break;
            default:
                $booking->payment_status = 'unpaid';
                break;
        }
        return $booking;
    }

    public function changeStatusPaymentAjax($request, $id)
    {
        $booking = $this->getBookingById($id);
        if($request->ajax()) {
            $booking->payment_status = $request->status;
            $booking->save();
        }
    }

    public function changeStatusPayment($status, $id)
    {
        $booking = $this->getBookingById($id);
        $booking->payment_status = $status;
        $booking->save();
    }

    public function changeStatus($status, $id)
    {
        $booking = $this->getBookingById($id);
        $booking->status = $status;
        $booking->save();
    }

    public function getTotalBooking() {
        return $this->where('payment_status', 2)->count();
    }

    public function totalPay() {
        return $this->where('payment_status', 2)->sum('total_price');
    }

    public function countProfitsInMonth($date) {
        return $this->where('payment_status', 2)->where(Booking::raw("DATE_FORMAT(created_at,'%m-%Y')"), $date)->sum('total_price');
    }

    public function countBookingInMonth($date) {
        return $this->where('payment_status', 2)->where(Booking::raw("DATE_FORMAT(created_at,'%m-%Y')"), $date)->count();
    }

    public function countTours($date) {
        return Tour::where(Booking::raw("DATE_FORMAT(created_at,'%m-%Y')"), $date)->count();
    }

    public function countContactInMonth($date) {
        return Contact::where(Booking::raw("DATE_FORMAT(created_at,'%m-%Y')"), $date)->count();
    }

    public function getDataChart($request) {
        $startArray = Carbon::today()->firstOfMonth()->subMonth(5);
        $arrayDate = [];
        $profits = [];
        $bookings = [];
        $contacts = [];
        $tours = [];
        for ($i = 0; $i < 6; $i++) {
            $arrayDate[$i] = $startArray->format('m-Y');
            $startArray->addMonth(1);
        }

        foreach ($arrayDate as $key => $date) {
            $profits[$key] = $this->countProfitsInMonth($date);
            $bookings[$key] = $this->countBookingInMonth($date);
            $contacts[$key] = $this->countContactInMonth($date);
            $tours[$key] = $this->countTours($date) + ($tours[$key - 1] ?? 0);
        }

        return response()->json([
            'arrayDate' => $arrayDate,
            'profits' => $profits,
            'bookings' => $bookings,
            'contacts' => $contacts,
            'tours' => $tours,
        ]);
    }

    public function getTopSellerTour($request) {

        $topToursData = $this->selectRaw('tour_id, count(tour_id) as count, sum(total_price) as sum')
            ->where('status', BOOKING_COMPLETE_STATUS);

        $now = Carbon::now();
        switch ($request->type) {
            case 'monthly':
                $currentMonth = $now->month;
                $topToursData = $topToursData->whereMonth('created_at', $currentMonth);
                break;
            case 'weekly':
                $weekStartDate = $now->startOfWeek()->format('Y-m-d');
                $weekEndDate = $now->endOfWeek()->format('Y-m-d');
                $topToursData = $topToursData->whereBetween('created_at', [$weekStartDate, $weekEndDate]);
                break;
            case 'daily':
                $topToursData = $topToursData->whereDate('created_at', $now->format('Y-m-d'));
                break;
            case 'yearly':
                $currentYear = $now->year;
                $topToursData = $topToursData->whereYear('created_at', $currentYear);
                break;
        }

        $topToursData = $topToursData->orderByRaw('count DESC, sum DESC')
            ->groupBy('tour_id')
            ->limit(5)
            ->get();
        $topToursAnalyticsData = $topToursData->map->only('count', 'sum','tour_id');
        $topToursID = $topToursData->pluck('tour_id')->toArray();
        $topTours = Tour::whereIn('id', $topToursID)->orderBy('id', 'desc')->get()->toArray();

        foreach ($topTours as $index => $tour) {
            if($tour['id'] == $topToursAnalyticsData[$index]['tour_id']) {
                $topTours[$index]['number_bookings'] = $topToursAnalyticsData[$index]['count'];
                $topTours[$index]['total_bookings'] = $topToursAnalyticsData[$index]['sum'];
            }
        }
        return $topTours;
    }
}
