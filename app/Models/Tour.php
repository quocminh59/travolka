<?php

namespace App\Models;

use App\Helper\Filterable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Tour extends Model
{
    use HasFactory, Filterable;
    protected $fillable = [
        'title',
        'slug',
        'destination_id',
        'type_tour_id',
        'duration',
        'price',
        'description',
        'image',
        'addtional_info',
        'map',
        'image_360',
        'video',
        'status',
        'trending'
    ];

    public function destination()
    {
        return $this->belongsTo(Destination::class);
    }

    public function type_tour()
    {
        return $this->belongsTo(TypeTour::class);
    }

    public function filterDestination($query, $request)
    {
        if($request->destination) {
            $slug = Str::slug($request->destination);
            return $query->whereHas('destination', function(Builder $q) use ($slug) {
               $q->where('slug', $slug);
            });
        }
    }

    public function getTourByID($id)
    {
        return $this->find($id);
    }

    public function getTourBySlug($slug)
    {
        return $this->where('slug', $slug)->first();
    }
}
