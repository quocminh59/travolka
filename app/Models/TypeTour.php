<?php

namespace App\Models;

use App\Helper\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeTour extends Model
{
    use HasFactory, Filterable;

    protected $fillable = [
        'title',
        'slug',
        'status'
    ];

    public function tours()
    {
        return $this->hasMany(Tour::class);
    }

    public function getTypeTourByID($id)
    {
        return $this->find($id);
    }

    public function getTypeTourBySlug($slug)
    {
        return $this->whereSlug($slug)->first();
    }

    public function getTypeTourByField($field)
    {
        
    }
}
