<?php

namespace App\Notifications;

use App\Models\Booking;
use App\Models\DeviceToken;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Kutia\Larafirebase\Messages\FirebaseMessage;

class HavingBooking extends Notification implements ShouldQueue
{
    use Queueable;

    public Booking $booking;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
       $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['firebase', 'database'];
    }

    public function toFirebase($notifiable)
    {
        $message = 'Customer ' . $this->booking->first_name . ' ordered booking tour';
        $fcmTokens = DeviceToken::all()->pluck('token')->toArray();
        return (new FirebaseMessage)
            ->withTitle('New Booking Order')
            ->withBody($message)
            ->withPriority('high')->asMessage($fcmTokens);
    }

    public function toDatabase($notifiable)
    {
        return [
            'message' => 'Customer ' . $this->booking->first_name . ' ordered booking tour',
            'order' => $this->booking->id
        ];
    }
}
