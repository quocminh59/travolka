<?php

namespace App\Listeners;

use App\Events\BookingPaidEvent;
use App\Notifications\BookingComplete;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class SendEmailCustomer implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(BookingPaidEvent $event): void
    {
        Notification::route('mail', 'quanquocminh@gmail.com')->notify(new BookingComplete($event->booking));
    }
}
