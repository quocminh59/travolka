<?php

namespace App\Listeners;

use App\Events\BookingPaidEvent;
use App\Models\User;
use App\Notifications\HavingBooking;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class SendAdminNotification
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     */
    public function handle(BookingPaidEvent $event): void
    {
        $admins = User::all();
        Notification::send($admins, new HavingBooking($event->booking));

    }
}
