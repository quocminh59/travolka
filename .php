<?php

function tinhgiaithua($n) {
    if($n <= 1) {
        return 1;
    }

    return $n * tinhgiaithua($n - 1);
}

$test = tinhgiaithua(0);

echo $test;
