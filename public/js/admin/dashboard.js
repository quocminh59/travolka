$(document).ready(function () {
    function viewTopSellerTours(url, type) {
        $.ajax({
            method: "POST",
            url: url,
            data: {
                type: type
            }
        }).done(function (response) {
            $('.card-tour-seller').find('.table-responsive').remove();
            $('.card-tour-seller').append(response);
        })
    }

    var urlRequest = $('.custom-select').data('uri');
    viewTopSellerTours(urlRequest, 'monthly');
    $('body').on('change', '.custom-select', function () {
        let predifined_range = $(this).val();
        viewTopSellerTours(urlRequest, predifined_range);
    });
});
