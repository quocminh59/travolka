<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('permissions')->insert([
            ['name' => 'tour.view'],
            ['name' => 'tour.list'],
            ['name' => 'tour.create'],
            ['name' => 'tour.update'],
            ['name' => 'tour.delete'],
        ]);

        $permissionIDs = Permission::all()->pluck('id');
        $adminRole = Role::where('name', 'admin')->first();
        $adminRole->permissions()->attach($permissionIDs);
    }
}
