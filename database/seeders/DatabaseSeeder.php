<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        //\App\Models\Tour::factory()->create();
//        for ($i = 0; $i < 1000; $i++) {
//            \App\Models\Tour::factory(1000)->create();
//        }

        $this->call([
            RoleTableSeeder::class,
            PermissionTableSeeder::class
        ]);
    }
}
