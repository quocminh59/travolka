<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Destination>
 */
class DestinationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $destinationTitle = fake()->words(1, true);

        return [
            'title' => $destinationTitle,
            'slug' => Str::slug($destinationTitle),
            'image' => fake()->imageUrl(640, 480, 'animals', 'true'),
            'status' => 1
        ];
    }
}
