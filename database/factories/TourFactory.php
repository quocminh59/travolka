<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Tour>
 */
class TourFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $tourTitle = fake()->sentence();
        $description = json_encode([
            'overview' => fake()->paragraph(),
            'included' => fake()->paragraph(),
            'departure' => fake()->paragraph()
        ]);
        $image = fake()->image('storage/app/public/upload', 640, 480);

        return [
            'title' => $tourTitle,
            'slug' => Str::slug($tourTitle),
            'destination_id' => fake()->numberBetween(1, 1000),
            'type_tour_id' => fake()->numberBetween(2, 4),
            'duration' => fake()->numberBetween(1, 5),
            'price' => 10000000,
            'description' => $description,
            'image' => explode("\\", $image)[1],
            'addtional_info' => fake()->paragraph(),
            'trending' => 1
        ];
    }
}



