<?php

return [
    /*
 * Messages Language Lines
 */

    'create_success' => ':Model added successfully',
    'create_failed' => ':Model added failed'
];
