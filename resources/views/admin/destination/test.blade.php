@extends('layouts.admin')

@section('title', 'Test Destination')

@section('breadcrumb')
    <div class="page-title">
        <h3>Create Destination</h3>
        {{ Breadcrumbs::render('create_destination') }}
    </div>
@endsection

@section('content')
    <div class="content-header">
        <h1>The page Test</h1>
    </div>
    <button type="button" class="btn-redirect" onclick="history.replaceState(null, '', 'some-page');">Click me</button>
@endsection

@section('script')
    <script>
        // $(window).bind('beforeunload', function(){
        //     console.log()
        //     $('.btn-redirect').click();
        // });
    </script>
@endsection
