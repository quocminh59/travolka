<div class="table-responsive">
    <table class="table v-middle">
        <thead>
        <tr class="bg-light">
            <th class="border-top-0">Products</th>
            <th class="border-top-0">Number of bookings</th>
            <th class="border-top-0">Earnings</th>
        </tr>
        </thead>
        <tbody>

        @if (count($topSellerTours) > 0)
            @foreach($topSellerTours as $tour)
                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <h4 class="mb-0 font-16">
                                <a href="{{ route('tour_detail', $tour['slug']) }}" target="_blank">{{ $tour['title'] }}</a>
                            </h4>
                        </div>
                    </td>
                    <td class="text-center"><strong>{{ $tour['number_bookings'] }}</strong></td>
                    <td>
                        <h5 class="mb-0">{{ number_format($tour['total_bookings']) }} đ</h5>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td>No Data</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
