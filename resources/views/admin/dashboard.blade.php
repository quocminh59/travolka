@extends('layouts.admin')

@section('breadcrumb')
<div class="page-title">
    <h3>Dashboard</h3>
    {{ Breadcrumbs::render('dashboard') }}
</div>
@endsection

{{--@section('content')--}}
{{--    <div class="row">--}}
{{--        <!-- Column -->--}}
{{--        <div class="col-12">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <div class="row mb-0 justify-content-between">--}}
{{--                        <!-- col -->--}}
{{--                        <div class="col-lg-2 col-md-6">--}}
{{--                            <div class="d-flex align-items-center">--}}
{{--                                <div class="mr-2"><span class="text-primary display-5">$</span></div>--}}
{{--                                <div>--}}
{{--                                    <h3 class="font-medium mb-0">{{ $totalPay }}</h3>--}}
{{--                                    <span>VND</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-lg-2 col-md-6">--}}
{{--                            <div class="d-flex align-items-center">--}}
{{--                                <div class="mr-2"><span class="text-success display-5"><i class="mdi mdi-airplane"></i></span></div>--}}
{{--                                <div>--}}
{{--                                    <h3 class="font-medium mb-0">{{ $totalBooking }}</h3>--}}
{{--                                    <span>Bookings</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="col-lg-2 col-md-6">--}}
{{--                            <div class="d-flex align-items-center">--}}
{{--                                <div class="mr-2"><span class="text-info display-5"><i class="mdi mdi-beach"></i></span></div>--}}
{{--                                <div>--}}
{{--                                    <h3 class="font-medium mb-0">{{ $totalTour }}</h3>--}}
{{--                                    <span>Tours</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="col-lg-2 col-md-6">--}}
{{--                            <div class="d-flex align-items-center">--}}
{{--                                <div class="mr-2"><span class="text-warning display-5"><i class="mdi mdi-map-marker"></i></span></div>--}}
{{--                                <div>--}}
{{--                                    <h3 class="font-medium mb-0">{{ $totalDestination }}</h3>--}}
{{--                                    <span>Destinations</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="col-lg-2 col-md-6">--}}
{{--                            <div class="d-flex align-items-center">--}}
{{--                                <div class="mr-2"><span class="text-danger display-5"><i class="mdi mdi-account-alert"></i></span></div>--}}
{{--                                <div>--}}
{{--                                    <h3 class="font-medium mb-0">{{ $totalContact }}</h3>--}}
{{--                                    <span>Contact</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="col-6">--}}
{{--            <div class="card card-tour-seller">--}}
{{--                <div class="card-body">--}}
{{--                    <!-- title -->--}}
{{--                    <div class="d-md-flex align-items-center">--}}
{{--                        <div>--}}
{{--                            <h4 class="card-title">Top Selling Tours</h4>--}}
{{--                            <h5 class="card-subtitle">Overview of Top Selling Tours</h5>--}}
{{--                        </div>--}}
{{--                        <div class="ml-auto">--}}
{{--                            <div class="dl">--}}
{{--                                <select class="custom-select" data-uri="{{ route('admin.getTopSellerTours') }}">--}}
{{--                                    <option value="{{ MONTHLY_TYPE }}" selected="">Monthly</option>--}}
{{--                                    <option value="{{ DAILY_TYPE }}">Daily</option>--}}
{{--                                    <option value="{{ WEEKLY_TYPE }}">Weekly</option>--}}
{{--                                    <option value="{{ YEARLY_TYPE }}">Yearly</option>--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="table-responsive">--}}
{{--                    <table class="table v-middle">--}}
{{--                        <thead>--}}
{{--                        <tr class="bg-light">--}}
{{--                            <th class="border-top-0">Products</th>--}}
{{--                            <th class="border-top-0">Number of bookings</th>--}}
{{--                            <th class="border-top-0">Earnings</th>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                            <tr>--}}
{{--                                <td>No data</td>--}}
{{--                            </tr>--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="col-6">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <!-- title -->--}}
{{--                    <div class="d-md-flex align-items-center">--}}
{{--                        <div>--}}
{{--                            <h4 class="card-title">Top View Tours</h4>--}}
{{--                            <h5 class="card-subtitle">Overview of Top View Tours</h5>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!-- title -->--}}
{{--                </div>--}}
{{--                <div class="table-responsive">--}}
{{--                    <table class="table v-middle">--}}
{{--                        <thead>--}}
{{--                        <tr class="bg-light">--}}
{{--                            <th class="border-top-0">Tours</th>--}}
{{--                            <th class="border-top-0">Total View</th>--}}

{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}

{{--                        @foreach($topViewTours as $tour)--}}
{{--                            <tr>--}}
{{--                                <td>--}}
{{--                                    <div class="d-flex align-items-center">--}}
{{--                                        <h4 class="mb-0 font-16">--}}
{{--                                            <a href="{{ route('tour_detail', $tour->slug) }}" target="_blank">{{ $tour->title }}</a>--}}
{{--                                        </h4>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                                <td class="text-center"><strong>{{ $tour->total_view }} views</strong></td>--}}

{{--                            </tr>--}}
{{--                        @endforeach--}}


{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="col-12">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <div class="row mb-0 justify-content-between">--}}
{{--                        <!-- col -->--}}
{{--                        <div class="col-lg-12">--}}
{{--                            <div class="card">--}}
{{--                                <div class="card-body analytics-info">--}}
{{--                                    <h4 class="card-title">Profits in 6 months ago</h4>--}}
{{--                                    <div id="profits_chart" style="height:400px;"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <div class="row mb-0 justify-content-between">--}}
{{--                        <!-- col -->--}}
{{--                        <div class="col-lg-12">--}}
{{--                            <div class="card">--}}
{{--                                <div class="card-body analytics-info">--}}
{{--                                    <h4 class="card-title">Infomation</h4>--}}
{{--                                    <div id="basic-line" style="height:400px;"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endsection--}}
{{--@section('script')--}}
{{--    <script src="{{ asset('js/admin/dashboard.js') }}"></script>--}}
{{--    <script>--}}
{{--        $(document).ready(function () {--}}
{{--            $.ajax({--}}
{{--                type: 'POST',--}}
{{--                url: "{{ route('admin.getDataChart') }}",--}}
{{--                success: function (response) {--}}
{{--                    lineChartProfits(response['arrayDate'], response['profits']);--}}
{{--                    lineChartInfor(response['arrayDate'], response['bookings'], response['contacts'], response['tours']);--}}
{{--                },--}}
{{--                error: function (xhr) {--}}

{{--                }--}}
{{--            })--}}
{{--        });--}}
{{--        function lineChartProfits(arrayDate, profits) {--}}
{{--            var myChart = echarts.init(document.getElementById('profits_chart'));--}}

{{--            // specify chart configuration item and data--}}
{{--            var option = {--}}
{{--                // Setup grid--}}
{{--                grid: {--}}
{{--                    left: '1%',--}}
{{--                    right: '2%',--}}
{{--                    bottom: '3%',--}}
{{--                    containLabel: true--}}
{{--                },--}}

{{--                // Add Tooltip--}}
{{--                tooltip : {--}}
{{--                    trigger: 'axis'--}}
{{--                },--}}

{{--                // Add Legend--}}
{{--                legend: {--}}
{{--                    data:['Profits']--}}
{{--                },--}}

{{--                // Enable drag recalculate--}}
{{--                calculable : true,--}}

{{--                // Horizontal Axiz--}}
{{--                xAxis : [--}}
{{--                    {--}}
{{--                        type : 'category',--}}
{{--                        boundaryGap : false,--}}
{{--                        data : arrayDate,--}}
{{--                    }--}}
{{--                ],--}}

{{--                // Vertical Axis--}}
{{--                yAxis : [--}}
{{--                    {--}}
{{--                        type : 'value',--}}
{{--                        axisLabel : {--}}
{{--                            formatter: '{value}'--}}
{{--                        }--}}
{{--                    }--}}
{{--                ],--}}

{{--                // Add Series--}}
{{--                series : [--}}
{{--                    {--}}
{{--                        name: 'Profits',--}}
{{--                        type:'line',--}}
{{--                        data: profits,--}}
{{--                        lineStyle: {--}}
{{--                            normal: {--}}
{{--                                width: 3,--}}
{{--                                shadowColor: 'rgba(0,0,0,0.1)',--}}
{{--                                shadowBlur: 10,--}}
{{--                                shadowOffsetY: 10--}}
{{--                            }--}}
{{--                        },--}}
{{--                    }--}}
{{--                ]--}}
{{--            };--}}
{{--            // use configuration item and data specified to show chart--}}
{{--            myChart.setOption(option);--}}
{{--        };--}}

{{--        function lineChartInfor(arrayDate, bookings, contacts, tours) {--}}
{{--            var myChart = echarts.init(document.getElementById('basic-line'));--}}

{{--            // specify chart configuration item and data--}}
{{--            var option = {--}}
{{--                // Setup grid--}}
{{--                grid: {--}}
{{--                    left: '1%',--}}
{{--                    right: '2%',--}}
{{--                    bottom: '3%',--}}
{{--                    containLabel: true--}}
{{--                },--}}

{{--                // Add Tooltip--}}
{{--                tooltip : {--}}
{{--                    trigger: 'axis'--}}
{{--                },--}}

{{--                // Add Legend--}}
{{--                legend: {--}}
{{--                    data:['Bookings', 'Contacts', 'Tours']--}}
{{--                },--}}

{{--                // Enable drag recalculate--}}
{{--                calculable : true,--}}

{{--                // Horizontal Axiz--}}
{{--                xAxis : [--}}
{{--                    {--}}
{{--                        type : 'category',--}}
{{--                        boundaryGap : false,--}}
{{--                        data : arrayDate,--}}
{{--                    }--}}
{{--                ],--}}

{{--                // Vertical Axis--}}
{{--                yAxis : [--}}
{{--                    {--}}
{{--                        type : 'value',--}}
{{--                        axisLabel : {--}}
{{--                            formatter: '{value}'--}}
{{--                        }--}}
{{--                    }--}}
{{--                ],--}}

{{--                // Add Series--}}
{{--                series : [--}}
{{--                    {--}}
{{--                        name: 'Bookings',--}}
{{--                        type: 'line',--}}
{{--                        data: bookings,--}}
{{--                        markPoint : {--}}
{{--                            data : [--}}
{{--                                {type : 'max', name: 'Max'},--}}
{{--                            ]--}}
{{--                        },--}}
{{--                        lineStyle: {--}}
{{--                            normal: {--}}
{{--                                width: 3,--}}
{{--                                shadowColor: 'rgba(0,0,0,0.1)',--}}
{{--                                shadowBlur: 10,--}}
{{--                                shadowOffsetY: 10--}}
{{--                            }--}}
{{--                        },--}}
{{--                    },--}}
{{--                    {--}}
{{--                        name: 'Contacts',--}}
{{--                        type: 'line',--}}
{{--                        data: contacts,--}}
{{--                        markPoint : {--}}
{{--                            data : [--}}
{{--                                {type : 'max', name: 'Max'},--}}
{{--                            ]--}}
{{--                        },--}}
{{--                        lineStyle: {--}}
{{--                            normal: {--}}
{{--                                width: 3,--}}
{{--                                shadowColor: 'rgba(0,0,0,0.1)',--}}
{{--                                shadowBlur: 10,--}}
{{--                                shadowOffsetY: 10--}}
{{--                            }--}}
{{--                        },--}}
{{--                    },--}}
{{--                    {--}}
{{--                        name: 'Tours',--}}
{{--                        type: 'line',--}}
{{--                        data: tours,--}}
{{--                        markPoint : {--}}
{{--                            data : [--}}
{{--                                {type : 'max', name: 'Max'},--}}
{{--                            ]--}}
{{--                        },--}}
{{--                        lineStyle: {--}}
{{--                            normal: {--}}
{{--                                width: 3,--}}
{{--                                shadowColor: 'rgba(0,0,0,0.1)',--}}
{{--                                shadowBlur: 10,--}}
{{--                                shadowOffsetY: 10--}}
{{--                            }--}}
{{--                        },--}}
{{--                    }--}}
{{--                ]--}}
{{--            };--}}
{{--            // use configuration item and data specified to show chart--}}
{{--            myChart.setOption(option);--}}
{{--        };--}}
{{--    </script>--}}
{{--@endsection--}}
