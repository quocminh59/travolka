<script src="{{ asset('xtreme/assets/libs/jquery/dist/jquery.min.js') }}"></script>
{{-- Yara DataTables --}}
<script src="{{ asset('xtreme/assets/extra-libs/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('xtreme/dist/js/pages/datatable/datatable-basic.init.js') }}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{ asset('xtreme/assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('xtreme/assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- apps -->
<script src="{{ asset('xtreme/dist/js/app.min.js') }}"></script>
<script src="{{ asset('xtreme/dist/js/app.init.js') }}"></script>
<script src="{{ asset('xtreme/dist/js/app-style-switcher.js') }}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{ asset('xtreme/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="{{ asset('xtreme/assets/extra-libs/sparkline/sparkline.js') }}"></script>
<!--Wave Effects -->
<script src="{{ asset('xtreme/dist/js/waves.js') }}"></script>
<!--Menu sidebar -->
<script src="{{ asset('xtreme/dist/js/sidebarmenu.js') }}"></script>
<!--Custom JavaScript -->
<script src="{{ asset('xtreme/dist/js/custom.js') }}"></script>
</script>
<script src="{{ asset('xtreme/assets/extra-libs/c3/d3.min.js') }}"></script>
<script src="{{ asset('xtreme/assets/extra-libs/c3/c3.min.js') }}"></script>
<script src="{{ asset('xtreme/dist/js/pages/dashboards/dashboard8.js') }}"></script>
<script src="{{ asset('vendor/sweet-alert/sweetalert2.min.js') }}"></script>
<script src="{{ asset('vendor/pannellum/pannellum.js') }}"></script>
<script src="{{ asset('xtreme/assets/libs/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('xtreme/dist/js/pages/forms/select2/select2.init.js') }}"></script>
<script src="{{ asset('xtreme/assets/libs/echarts/dist/echarts-en.min.js') }}"></script>
<script src="{{ asset('xtreme/dist/js/pages/echarts/pie-doughnut/pie-doghnut.js') }}"></script>
<script src="{{ asset('js/config.js') }}"></script>
<script src="{{ asset('js/function.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/toastr@2.1.4/build/toastr.min.js"></script>
<script>
    @if (Session::has('message'))
        toastr.success('{{ session('message') }}');
    @endif

    @if (Session::has('error'))
        toastr.error('{{ session('error') }}');
    @endif
</script>
<script>
    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js"></script>

<script>
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    const firebaseConfig = {
        apiKey: "AIzaSyAISax8IbU5T0LgWxU39SUBR3pP6kwUoKQ",
        authDomain: "push-notification-eee67.firebaseapp.com",
        projectId: "push-notification-eee67",
        storageBucket: "push-notification-eee67.appspot.com",
        messagingSenderId: "919966838612",
        appId: "1:919966838612:web:6acf5a3cd49fb650f562c1",
        measurementId: "G-9R0MWZNJ46"
    };

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);

    const messaging = firebase.messaging();

    function initFirebaseMessagingRegistration() {
        messaging.requestPermission().then(function () {
            return messaging.getToken()
        }).then(function(token) {
            console.log(token);
            $.ajax({
                url: '{{ route("fcmToken") }}',
                type: 'POST',
                data: {
                    token: token
                },
                success: function (response) {
                    console.log('token saved')
                },
                error: function (err) {
                    console.log(err)
                    console.log('User Chat Token Error'+ err);
                },
            });

        }).catch(function (err) {
            console.log(`Token Error :: ${err}`);
        });
    }

    initFirebaseMessagingRegistration();

    messaging.onMessage(function({data:{body,title}}){
        console.log(title, {body});
        //new Notification(title, {body});
    });
</script>
@yield('script')
