@extends('layouts.app')

@section('title', 'NgaoDuVietNam')

@section('logo')
    <img src="{{ asset('assets/images/logo-3.png') }}" alt="">
@endsection

{{-- import css --}}
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('xtreme/assets/libs/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/owlcarousel/dist/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/owlcarousel/dist/assets/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/client/homepage/app.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/client/homepage/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('css/client/list_tour/tour.css') }}">
@endsection


{{-- main-content --}}
@section('content')
    <div class="container wrap-content width-default">
        <div class="bread-crumb d-flex">
            <a href="#" class="first-br" style="margin-right: 5px; text-decoration: none;">Home</a>
            <div class="next-br">
                <img src="assets/images/Ellipse.png" alt="">
                <a href="#" style="text-decoration: none;">Tours</a>
            </div>
        </div>

        <div class="slide-type mt-5 mb-5">
            <div class="slide-header">
                <div class="sl-hd-title">
                    Danh sách các địa điểm du lịch hấp dẫn
                </div>
            </div>

            <div class="sl-content">
                <div class="owl-carousel owl-theme owl-loaded " id="slide-1">
                    <div class="owl-stage-outer">
                        <div class="owl-stage">
                            @foreach ($destinations as $destination)
                                @if($destination->status == 1)
                                    <div class="owl-item sl1-item">
                                        <img src="{{ asset('storage/upload/'.$destination->image) }}" alt="">
                                        <a class="location" href="#">{{ $destination->title }}</a>
                                        <p class="experience">24 experience</p>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>
@endsection

{{-- import js --}}
@section('script')
    <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.js') }}"></script>
    <script src="{{ asset('vendor/owlcarousel/dist/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/client/index.js') }}"></script>
    <script src="{{ asset('xtreme/assets/libs/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('xtreme/dist/js/pages/forms/select2/select2.init.js') }}"></script>
@endsection
